# https://www.nginx.com/blog/mitigating-ddos-attacks-with-nginx-and-nginx-plus/

limit_req_zone $binary_remote_addr zone=one:10m rate=30r/m;
limit_conn_zone $binary_remote_addr zone=addr:10m;

# first we declare our upstream server, which is our Gunicorn application
upstream app {
  # docker will automatically resolve this to the correct address
  # because we use the same name as the service: "web"
  server web:8000 max_conns=300;
}

# now we declare our main server
server {
  listen 80;
  server_name localhost;

  client_body_timeout 5s;
  client_header_timeout 5s;

  client_max_body_size 20M;

  location = /favicon.ico {
    return 204;
    access_log     off;
    log_not_found  off;
  }


  location = /robots.txt {
    add_header Content-Type text/plain;
    alias  /etc/nginx/conf.d/robots.txt;
  }

  location / {
    limit_conn addr 10;

    # everything is passed to Gunicorn
    proxy_pass http://app;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_redirect off;
  }
}
